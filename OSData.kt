package com.example.aplikasicardrecyclerview

object OSData {

    private val osName = arrayOf(
        "Symbian",
        "Windows",
        "Blackberry",
        "Sun OS",
        "FreeBSD",
        "Solaris",
        "Android"
    )

    private val osInfo = arrayOf(
        "Symbian adalah sistem operasi yang dikembangkan oleh Symbian Ltd. yang dirancang untuk peralatan bergerak (mobile).",
        "Microsoft Windows atau Windows adalah sistem operasi yang dikembangkan oleh Microsoft.",
        "BlackBerry OS adalah sistem operasi seluler yang dikembangkan oleh BlackBerry Limited.",
        "SunOS adalah sistem operasi Unix yang dikembangkan oleh Sun Microsystems untuk sistem komputer workstation dan server.",
        "FreeBSD adalah sebuah sistem operasi turunan Unix AT&T yang berasal Berkeley Software Distribution (BSD).",
        "Solaris adalah sistem operasi Unix pertama yang dikembangkan oleh Sun Microsystems.",
        "Android adalah sistem operasi seluler yang telah dimodifikasi dari kernel Linux yang dikembangkan oleh Open Handset Alliance, kemudian Google."
    )

    private val osPhoto = intArrayOf(
        R.drawable.a_symbian_os_icon,
        R.drawable.b_windows_os_icon,
        R.drawable.c_blackberry_os_icon,
        R.drawable.d_sun_os_icon,
        R.drawable.e_freebsd_os_icon,
        R.drawable.f_solaris_os_icon,
        R.drawable.g_android_os_icon
    )

    val listData: ArrayList<OS>
        get() {
            val list = arrayListOf<OS>()
            for (position in osName.indices){
                val os = OS()
                os.name = osName[position]
                os.info = osInfo[position]
                os.photo = osPhoto[position]
                list.add(os)
            }
            return list
        }
}